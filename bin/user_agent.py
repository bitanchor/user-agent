#!/usr/bin/env python3

'''
@blame R. Matt McCann
@brief Facilitates the secure container checking and communicating with the api
@copyright &copy; 2017 Human-Proof Corp.
'''

import signal
import sys

sys.path.insert(0, 'src')

import human_proof.user_agent.lock_watcher as lock_watcher # noqa

from human_proof.user_agent.file_access_request_listener import FileAccessRequestListener # noqa


# Launch the file access request listener
listener = FileAccessRequestListener()
listener.start()


# Handle to shutdown cleanly
def kill_signal_handler(signal, frame):
    print("Caught kill signal. Shutting down...")

    # Stop the listener
    listener.request_shutdown()

    # Stop the file watchers
    for watcher in lock_watcher.lock_watchers:
        watcher.request_shutdown()

    sys.exit(0)


# Attach the signal listeners
signal.signal(signal.SIGINT, kill_signal_handler)
signal.signal(signal.SIGTERM, kill_signal_handler)
signal.pause()
