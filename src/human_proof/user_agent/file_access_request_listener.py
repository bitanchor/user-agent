'''
@blame R. Matt McCann
@brief Listens to the file access FIFO for file access requests
@copyright &copy; 2017 Human-Proof Corp.
'''

import errno
import os
import traceback

from subprocess import Popen

from human_proof.api.client import File, User, FileAccess
from human_proof.misc.daemon import Daemon
from human_proof.user_agent.lock_watcher import LockWatcher


class FileAccessRequestListener(Daemon):
    ''' Listens to the file access FIFO for file access requests. '''

    fifo_name = '/tmp/human-proof-file-access'
    shutdown_request = 'shutdown\n'

    # This is hard-coded for the demo
    user_id = '0'

    def __init__(self):
        super(FileAccessRequestListener, self).__init__()

    def cleanup_fifo(self):
        ''' Cleans up the request fifo. '''
        # Close the opened fifo
        #os.close(self.fifo)

        # Unlink the fifo
        #os.unlink(self.fifo_name)

    def handle_request(self, request):
        ''' Handle the file access request. '''
        secure_container_pid = request.split()[0]
        file_id = request.split()[1]

        # Fetch a fresh copy of the user account
        user = User.read(self.user_id)

        # If the user hasn't authenticated yet
        if not user.is_authenticated:
            # Dispaly a pretty message to the user
            print("User not yet authenticated - opening auth window!")

            # Open the authentication window
            auth_window = Popen(["firefox", os.environ['HUMANPROOF_AUTH_URL']])

            # Wait for the auth window to be closed
            auth_window.wait()

        # If the user is locked
        if user.is_locked:
            # Report the access request
            FileAccess.create(file_id, self.user_id, False)

            # Kill the requesting secure container
            self.kill_container(secure_container_pid)

            # Display a pretty message to the user
            print("Access denied - user account is locked!")

            # We are done handling the request
            return

        # Fetch the file record
        file = File.read(file_id)

        # If the file is locked
        if file.is_locked:
            # Report the access request
            FileAccess.create(file_id, self.user_id, False)

            # Kill the requesting secure container
            self.kill_container(secure_container_pid)

            # Display a pretty message to the user
            print("Access denied - file is locked!")

            # We are done handling the request
            return

        # Report the access request
        FileAccess.create(file_id, self.user_id, True)

        # Touch an "approval" file to indicate to the process that it can proceed
        os.system('touch /tmp/approved.%s' % secure_container_pid)

        # Launch a lock watcher for this secure container
        lock_watcher = LockWatcher.construct(self.user_id, secure_container_pid, file_id)
        lock_watcher.start()

    def kill_container(self, secure_container_pid):
        ''' Kills the secure container. '''
        os.system('kill %s' % secure_container_pid)

    #def request_shutdown(self):
    #    ''' @see Daemon.request_shutdown '''
    #    # Request the thread shutdown
    #    Daemon.request_shutdown(self)

    #    # Push the shutdown request message to ensure the listener isn't blocking
    #    #self.fifo.write(self.shutdown_request)

    def run_daemon(self):
        ''' Listen for file access requests. '''
        # Create the request fifo
        self.setup_file_access_fifo()

        try:
            # While the listener isn't shutting down
            while True:
                request = ''

                try:
                    # Open the created fifo
                    fifo = os.open(self.fifo_name, os.O_NONBLOCK | os.O_RDONLY)

                    # Read the next request
                    request = os.read(fifo, 1024).decode('utf-8').rstrip()

                    # Close the fifo
                    os.close(fifo)
                except:
                    pass

                if len(request) > 0:
                    print("Request:", request)

                    # Handle the request
                    try:
                        self.handle_request(request)
                    except Exception as ex:
                        traceback.print_exc()

                # If we are shutting down
                if self.is_shutting_down:
                    # Shutdown
                    break

        finally:
            # Clean up the request fifo
            self.cleanup_fifo()

    def setup_file_access_fifo(self):
        ''' Sets up the file access request FIFO and opens it. '''
        try:
            # Delete the fifo if it already exists
            os.unlink(self.fifo_name)
        except:
            pass

        try:
            # Try to create the fifo
            os.mkfifo(self.fifo_name)
        except OSError as oe:
            # If this is an error other than the fifo already existing
            if oe.errno != errno.EEXIST:
                raise
