'''
@blame R. Matt McCann
@brief Unit tests for the FileAccessRequestListener
@copyright &copy; 2017 Human-Proof Corp.
'''

import os
import subprocess

from unittest import mock

from human_proof.api.client import File, FileAccess, User
from human_proof.user_agent.file_access_request_listener import FileAccessRequestListener
from human_proof.user_agent.lock_watcher import LockWatcher


class TestFileAccessRequestListener(object):
    ''' Unit tests for the FileAccessRequestListener. '''

    def test_cleanup_fifo(self):
        ''' Tests that the FIO is correctly cleaned up. '''
        fifo_name = '/tmp/human-proof-file-access_test_cleanup_fifo'

        # Setup the listener
        listener = FileAccessRequestListener()
        listener.fifo_name = fifo_name

        # Create the fifo
        listener.setup_file_access_fifo()

        # Cleanup the fifo
        listener.cleanup_fifo()

        # Check that the fifo was properly cleaned up
        assert not os.path.exists(fifo_name)

    @mock.patch('human_proof.user_agent.file_access_request_listener.FileAccessRequestListener.kill_container')
    @mock.patch('human_proof.user_agent.file_access_request_listener.User.read')
    @mock.patch('human_proof.user_agent.file_access_request_listener.FileAccess.create')
    @mock.patch('human_proof.user_agent.file_access_request_listener.Popen')
    def test_handle_request__user_not_authenticated(self, popen, file_access_create, user_read, kill_container):
        ''' Tests handling a secure container request when the user has not yet authenticated. '''
        # Mock out the listener inputs
        popen.return_value = mock.create_autospec(subprocess.Popen)
        user_read.return_value = mock.create_autospec(User)
        user_read.return_value.is_locked = True
        user_read.return_value.is_authenticated = False
        os.environ['HUMANPROOF_AUTH_URL'] = 'http://localhost'

        # Process the request
        listener = FileAccessRequestListener()
        listener.handle_request('sec_pid file_id')

        # Check that the auth window was opened
        popen.assert_called_with(["chrome", "http://localhost"])

    @mock.patch('human_proof.user_agent.file_access_request_listener.FileAccessRequestListener.kill_container')
    @mock.patch('human_proof.user_agent.file_access_request_listener.User.read')
    @mock.patch('human_proof.user_agent.file_access_request_listener.FileAccess.create')
    def test_handle_request__user_is_locked(self, file_access_create, user_read, kill_container):
        ''' Tests handling a secure container request when the user account is locked. '''
        # Mock out the listener inputs
        user_read.return_value = mock.create_autospec(User)
        user_read.return_value.is_locked = True
        user_read.return_value.is_authenticated = True

        # Process the request
        listener = FileAccessRequestListener()
        listener.handle_request('sec_pid file_id')

        # Check that the file access record was created
        file_access_create.assert_called_with('file_id', listener.user_id, False)

        # Check that the secure container was killed
        kill_container.assert_called_with()

    @mock.patch('human_proof.user_agent.file_access_request_listener.FileAccessRequestListener.kill_container')
    @mock.patch('human_proof.user_agent.file_access_request_listener.User.read')
    @mock.patch('human_proof.user_agent.file_access_request_listener.FileAccess.create')
    @mock.patch('human_proof.user_agent.file_access_request_listener.File.read')
    def test_handle_request__file_is_locked(self, file_read, file_access_create, user_read, kill_container):
        ''' Tests handling a secure container request when the file is locked. '''
        # Mock out the listener inputs
        user_read.return_value = mock.create_autospec(User)
        user_read.return_value.is_locked = False
        user_read.return_value.is_authenticated = True
        file_read.return_value = mock.create_autospec(File)
        file_read.return_value.is_locked = True

        # Process the request
        listener = FileAccessRequestListener()
        listener.handle_request('sec_pid file_id')

        # Check that the file access record was created
        file_access_create.assert_called_with('file_id', listener.user_id, False)

        # Check that the secure container was killed
        kill_container.assert_called_with()

    @mock.patch('human_proof.user_agent.file_access_request_listener.FileAccessRequestListener.kill_container')
    @mock.patch('human_proof.user_agent.file_access_request_listener.User.read')
    @mock.patch('human_proof.user_agent.file_access_request_listener.FileAccess.create')
    @mock.patch('human_proof.user_agent.file_access_request_listener.File.read')
    @mock.patch('human_proof.user_agent.file_access_request_listener.LockWatcher.construct')
    def test_handle_request__approved(self, construct, file_read, file_access_create, user_read, kill_container):
        ''' Tests handling a secure container request when the access is granted. '''
        # Mock out the listener inputs
        user_read.return_value = mock.create_autospec(User)
        user_read.return_value.is_locked = False
        user_read.return_value.is_authenticated = True
        file_read.return_value = mock.create_autospec(File)
        file_read.return_value.is_locked = False
        construct.return_value = mock.create_autospec(LockWatcher)

        # Process the request
        listener = FileAccessRequestListener()
        listener.handle_request('test_handle_request__approved file_id')

        # Check that the file access record was created
        file_access_create.assert_called_with('file_id', listener.user_id, True)

        # Check that the approval touch occurred
        assert os.path.exists('/tmp/approved.test_handle_request__approved') is True

        # Check that the lock watcher was spawned
        construct.assert_called_with(listener.user_id, 'test_handle_request__approved', 'file_id')
        construct.return_value.start.assert_called_with()

    def test_setup_file_access_fifo(self):
        ''' Tests that the FIFO is correctly created and opened. '''
        fifo_name = '/tmp/human-proof-file-access_test_setup_file_access_fifo'

        # Setup the listener
        listener = FileAccessRequestListener()
        listener.fifo_name = fifo_name

        # Delete any lingering FIFOs from previous tests
        try:
            os.unlink(fifo_name)
        except:
            pass

        # Run the function under test
        listener.setup_file_access_fifo()

        # Check the results
        assert os.path.exists(fifo_name)
