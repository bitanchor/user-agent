'''
@blame R. Matt McCann
@brief Unit tests for the lock watcher
@copyriight &copy; 2017 Human-Proof Corp.
'''

from unittest import mock

import human_proof.user_agent.lock_watcher as lock_watcher

from human_proof.api.client import File, User


class TestLockWatcher(object):
    ''' Unit tests for the LockWatcher class. '''

    user_id = 'user_id'
    sec_pid = 'secure_container_pid'
    file_id = 'file_id'

    @mock.patch('human_proof.user_agent.lock_watcher.Daemon.__init__', autospec=True)
    def test_init(self, daemon_init):
        ''' Tests that the super init is called as expected. '''
        watcher = lock_watcher.LockWatcher()

        daemon_init.assert_called_with(watcher)

    def test_construct(self):
        ''' Tests constructing a new lock watcher. '''
        # Construct the lock watcher
        watcher = lock_watcher.LockWatcher.construct(self.user_id, self.sec_pid, self.file_id)

        # Check the watcher was initialized as expected
        assert watcher.user_id == self.user_id
        assert watcher.secure_container_pid == self.sec_pid
        assert watcher.file_id == self.file_id

        # Check that the watcher was added to the master listener
        assert watcher in lock_watcher.lock_watchers

    @mock.patch('human_proof.user_agent.lock_watcher.File.read')
    @mock.patch('human_proof.user_agent.lock_watcher.User.read')
    @mock.patch('human_proof.user_agent.lock_watcher.LockWatcher.kill_container')
    def test_run_daemon__nothing_locked(self, kill_container, user_read, file_read):
        ''' Tests running the lock watcher when nothing is locked. '''
        # Mock out the watcher inputs
        file_read.return_value = mock.create_autospec(File)
        file_read.return_value.is_locked = False
        user_read.return_value = mock.create_autospec(User)
        user_read.return_value.is_locked = False

        # Construct the lock watcher
        watcher = lock_watcher.LockWatcher.construct(self.user_id, self.sec_pid, self.file_id)

        # Start the watcher
        watcher.is_shutting_down = True
        watcher.start()
        watcher.join()

        # Check the results
        watcher.kill_container.assert_not_called()

    @mock.patch('human_proof.user_agent.lock_watcher.File.read')
    @mock.patch('human_proof.user_agent.lock_watcher.User.read')
    @mock.patch('human_proof.user_agent.lock_watcher.LockWatcher.kill_container')
    def test_run_daemon__user_is_locked(self, kill_container, user_read, file_read):
        ''' Tests running the lock watcher when the user becomes locked. '''
        # Mock out the watcher inputs
        file_read.return_value = mock.create_autospec(File)
        file_read.return_value.is_locked = False
        user_read.return_value = mock.create_autospec(User)
        user_read.return_value.is_locked = True

        # Construct the lock watcher
        watcher = lock_watcher.LockWatcher.construct(self.user_id, self.sec_pid, self.file_id)

        # Start the watcher
        watcher.is_shutting_down = True
        watcher.start()
        watcher.join()

        # Check the results
        kill_container.assert_called_with()

    @mock.patch('human_proof.user_agent.lock_watcher.File.read')
    @mock.patch('human_proof.user_agent.lock_watcher.User.read')
    @mock.patch('human_proof.user_agent.lock_watcher.LockWatcher.kill_container')
    def test_run_daemon__file_is_locked(self, kill_container, user_read, file_read):
        ''' Tests running the lock watcher when the file becomes locked. '''
        # Mock out the watcher inputs
        file_read.return_value = mock.create_autospec(File)
        file_read.return_value.is_locked = True
        user_read.return_value = mock.create_autospec(User)
        user_read.return_value.is_locked = False

        # Construct the lock watcher
        watcher = lock_watcher.LockWatcher.construct(self.user_id, self.sec_pid, self.file_id)

        # Start the watcher
        watcher.is_shutting_down = True
        watcher.start()
        watcher.join()

        # Check the results
        kill_container.assert_called_with()
