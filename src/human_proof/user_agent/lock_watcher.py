'''
@blame R. Matt McCann
@brief Watches the user and file records to see if a lock has occurred, and kills the secure container as needed
@copyright &copy; 2017 Human-Proof Corp.
'''

import os
import time

from human_proof.api.client import File, User
from human_proof.misc.daemon import Daemon

lock_watchers = []


class LockWatcher(Daemon):
    ''' Watches the user and file records to see if a lock has occurred, and kills the secure container as needed. '''

    def __init__(self):
        super(LockWatcher, self).__init__()

    @classmethod
    def construct(cls, user_id, secure_container_pid, file_id):
        '''
        Constructs a new lock watcher to monitor the secure container.

        @param user_id Tracking ID of the user who spawned the secure container pid
        @param secure_container_pid Process ID of the secure container pid
        @param file_id Tracking ID of the file opened in the secure container pid
        @returns The constructed file watcher
        '''
        lock_watcher = LockWatcher()
        lock_watcher.user_id = user_id
        lock_watcher.secure_container_pid = secure_container_pid
        lock_watcher.file_id = file_id

        # Keep track of the spawned lock watchers
        lock_watchers.append(lock_watcher)

        return lock_watcher

    def kill_container(self):
        ''' Kills the secure container. '''
        os.system('kill %s' % self.secure_container_pid)

    def run_daemon(self):
        ''' Watches the state and kills the secure container as needed. '''
        while True:
            # Fetch updated copies of the user and file records
            file = File.read(self.file_id)
            user = User.read(self.user_id)

            # If the user is locked
            if user.is_locked:
                # Display a pretty message to the user
                print("User account has been locked - killing secure container")

                # Kill the secure container
                self.kill_container()

                # Shut down the watcher
                self.is_shutting_down = True

            # If the file is locked
            if file.is_locked:
                # Display a pretty message to the user
                print("File has been locked - killing secure container")

                # Kill the secure container
                self.kill_container()

                # Shut down the watcher
                self.is_shutting_down = True

            # If the watcher is shutting down
            if self.is_shutting_down:
                # Shut down
                break

            # Wait a bit before checking again
            time.sleep(1)
