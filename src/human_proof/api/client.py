'''
@blame R. Matt McCann
@brief Client for the Human-Proof API
@copyright &copy; 2017 Human-Proof Corp.
'''

import json
import os
import requests

from datetime import datetime


class File(object):
    ''' Client for the File API. '''

    @classmethod
    def read(cls, id):
        '''
        Read the file record.

        @returns File instance matching the state of the file record
        '''
        url = '%s/file/%s/' % (os.environ['HUMANPROOF_API_HOST'], id)

        # Make the request
        response = requests.get(url)

        # If an error occurred
        if response.status_code != 200:
            raise Exception("Request to %s failed with code %s!" % (url, response.status_code))

        # Create a File instance populated with the response values
        file = File()
        for key, value in response.json().items():
            setattr(file, key, value)

        return file


class FileAccess(object):
    ''' Client for the FileAccess API. '''

    @classmethod
    def create(cls, file_id, user_id, status):
        '''
        Creates a new file access record.
        '''
        url = '%s/file/access/' % (os.environ['HUMANPROOF_API_HOST'])

        # Make the request
        response = requests.post(url, json={
            "action": "read",
            "context": "Nationwide VPN",
            "file_id": file_id,
            "file_name": "CustomerPII.doc",
            "location_lat": 39.967292,
            "location_lon": -83.006659,
            "time": datetime.now().strftime('%Y/%m/%d %H:%M:%S'),
            "user_id": user_id,
            "user_name": "Matt McCann",
            "was_permitted": status
        })

        # If an error occurred
        if response.status_code != 200:
            raise Exception("Request to %s failed with code %s!" % (url, response.status_code))


class User(object):
    ''' Client for the User API. '''

    @classmethod
    def read(cls, id):
        '''
        Read the user record.

        @returns User instance matching the state of the user record
        '''
        url = '%s/user/%s/' % (os.environ['HUMANPROOF_API_HOST'], id)

        # Make the request
        response = requests.get(url)

        # If an error occurred
        if response.status_code != 200:
            raise Exception("Request to %s failed with code %s!" % (url, response.status_code))

        # Create a User instance populated with the response values
        user = User()
        for key, value in response.json().items():
            setattr(user, key, value)

        return user
