'''
@blame R. Matt McCann
@brief Thread that runs continously and must be told to shut down
@copyright &copy; 2017 Human-Proof Corp.
'''

from threading import Thread


class Daemon(Thread):
    ''' Thread that runs continuously and must be told to shut down. '''

    def __init__(self):
        super(Daemon, self).__init__()

        self.daemon = True
        self.is_shutdown = True
        self.is_shutting_down = False
        self.is_running = False

    def request_shutdown(self):
        ''' Requests the daemon to cleanly shut down. '''
        # Tell the deemon to shut down
        self.is_shutting_down = True

    def run(self):
        ''' {@inheritDocs} '''
        assert self.is_shutdown
        assert not self.is_running

        # Mark the daemon state as not shutdown and not shutting down
        self.is_shutdown = False

        # Run the daemon's work function
        self.run_daemon()

        # Mark the daemon state as not running and shut down
        self.is_running = False
        self.is_shutdown = True
        self.is_shutting_down = False

    def run_daemon(self):
        ''' Interface for extending classes to implement the daemon's run behavior. '''
        raise NotImplementedError()
